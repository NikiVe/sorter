<?php

use DataStructure\ArrayList;
use Sorter\NumberGenerator;
use Sorter\Runner;
use Tester\Tester;

include_once __DIR__ . '/vendor/autoload.php';

$g = new NumberGenerator(100000);
$writer = new \Sorter\file\Writer(__DIR__ . '/file.txt');
$g->saveToBinFile($writer);
unset($writer);

$sorter = new \Sorter\ExternalSort(__DIR__ . '/file.txt');
$sorter->setPartSive(1024);

$sT = microtime(true);
$sorter->sort();
echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;