<?php

use DataStructure\ArrayList;
use Sorter\NumberGenerator;
use Sorter\Runner;
use Tester\Tester;

include_once __DIR__ . '/vendor/autoload.php';

ini_set("memory_limit", "6000M");
Tester::start(__DIR__ . '/sorting-tests/0.random/', new Runner(new \Sorter\BucketSort()), 1);