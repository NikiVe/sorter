<?php

namespace Sorter;

class FastShellSort extends ShellSort
{
    public function sort(int $size, array $array) {
        $gaps = call_user_func($this->func, $size);

        foreach ($gaps as $gap) {
            for ($i = 0; $i + $gap < $size; $i++) {
                $j = $i + $gap;
                $tmp = $array[$j];

                while ($j - $gap >= 0 && $array[$j - $gap] > $tmp) {
                    $array[$j] = $array[$j - $gap];
                    $j -= $gap;
                }
                $array[$j] = $tmp;
            }
        }
        return $array;
    }
}