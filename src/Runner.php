<?php


namespace Sorter;


use Tester\TestFunc;

class Runner implements TestFunc
{
    protected $obj;

    public function __construct(ISort $obj) {
        $this->obj = $obj;
    }

    public function run(string $values): string {
        $arVal = explode(PHP_EOL, $values);
        $result = $this->obj->sort((int)$arVal[0], explode(' ', $arVal[1]));
        return implode(' ', $result);
    }
}