<?php


namespace Sorter;


class InsertionSort extends Sorter
{
    public function sort(int $size, array $array) {
        for ($i = 1; $i < $size; $i++) {
            for ($j = $i; $j > 0 && $array[$j] < $array[$j - 1]; $j--) {
                $array = self::swap($array, $j, $j - 1);
            }
        }
        return $array;
    }
}