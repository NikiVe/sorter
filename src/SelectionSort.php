<?php


namespace Sorter;


class SelectionSort extends Sorter
{
    public function sort(int $size, array $array) {
        for ($i = 0; $i < $size; $i++) {
            $mP = self::getMin($array, $i, $size - 1);
            $array = self::swap($array, $i, $mP);
        }
        return $array;
    }

    public static function getMin(array $array, int $sP, int $end) {
        $p = $sP;
        for ($i = $sP + 1; $i <= $end; $i++) {
            if ($array[$p] > $array[$i]) {
                $p = $i;
            }
        }
        return $p;
    }
}