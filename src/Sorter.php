<?php


namespace Sorter;


abstract class Sorter implements ISort
{
    public static function swap(array $array, int $i, int $j) {
        $temp = $array[$i];
        $array[$i] = $array[$j];
        $array[$j] = $temp;
        return $array;
    }
}