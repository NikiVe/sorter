<?php


namespace Sorter;


class MergeSort implements ISort
{
    protected $array = [];
    protected $size = 0;

    public function sort(int $size, array $array){
        $this->array = $array;
        $this->size = $size;

        $l = 0;
        $r = $this->size - 1;
        $this->run($l, $r);
        return $this->array;
    }

    protected function run(int &$l, int &$r) {
        if ($l === $r) {
            return;
        }
        $c = (int)(($l + $r) / 2);
        $this->run($l, $c);
        $c++;
        $this->run($c, $r);
        $this->array = self::merge($l, $c, $r, $this->array);
//        print_r(($r / $this->size)  * 100 . PHP_EOL);
    }

    public static function merge(int $l, int $c, int $r, array $array) {
        $pa1 = $l;
        $pa2 = $c;

        $result = [];

        while ($pa1 < $c && $pa2 < $r + 1) {
            if ($array[$pa1] <= $array[$pa2]) {
                $result[] = $array[$pa1++];
            } else {
                $result[] = $array[$pa2++];
            }
        }
        if ($pa1 >= $c) {
            while ($pa2 < $r) {
                $result[] = $array[$pa2++];
            }
        } else {
            while ($pa1 < $c) {
                $result[] = $array[$pa1++];
            }
        }

        foreach ($result as $i => $v) {
            $array[$l + $i] = $v;
        }

        return $array;
    }
}