<?php


namespace Sorter;


use Sorter\file\Writer;

class NumberGenerator
{
    protected $N;

    public function __construct(int $n) {
        $this->N = $n;
    }

    public function saveToBinFile(Writer $writer) {
        for ($i = 0; $i < $this->N; $i++) {
            $num = rand(0, 65535);
            $writer->saveItem($num);
        }
    }

    public static function getOneE6() {
        return 10 ** 6;
    }

    public static function getOneE5() {
        return 10 ** 5;
    }
}