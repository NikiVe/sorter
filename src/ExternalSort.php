<?php


namespace Sorter;


use Sorter\file\MergeFile;
use Sorter\File\Reader;
use Sorter\file\Writer;

class ExternalSort
{
    protected $array = [];
    protected $size = 0;

    protected $partSize = 32;

    protected $sorter;
    /**
     * Файл с числами для сортировки
     * @var Reader
     */
    protected $mainFile;
    protected MergeFile $mergeFile;

    /**
     * Номер созданного файла
     * @var int
     */
    protected $counter = 0;

    public function __construct(string $path) {
        $this->sorter = new ShellSort();
        $this->mainFile = new Reader($path);
        $this->mergeFile = new MergeFile(__DIR__ . '/parts/merge.bin');
    }

    public function sort(){
        $this->size = $this->mainFile->getElementsCount();

        $l = 0;
        $r = $this->size - 1;
        $this->divider($l, $r);
        $this->merger();
    }

    public function setPartSive($n) {
        $this->partSize = $n;
    }

    protected function merger() {
        for ($i = 0; $i < $this->counter; $i++) {
            $p1 = __DIR__ . "/parts/$i.bin";
            if  (file_exists($p1)) {
                $ar = (new Reader($p1))->getAll();
                $mAr = $this->mergeFile->get();

                $this->mergeFile->save(self::merge($ar, $mAr));

                unlink($p1);
            }
        }
    }

    protected function divider(int &$l, int &$r) {
        if ($l === $r) {
            return;
        }

        $size = $r - $l;
        if ($size <= $this->partSize) {
            $res = $this->sorter->sort($size, $this->mainFile->getNext($size));
            (new Writer(__DIR__ . "/parts/$this->counter.bin"))->saveArray($res);
            $this->counter++;
            return;
        }

        $c = (int)(($l + $r) / 2);
        $this->divider($l, $c);
        $c++;
        $this->divider($c, $r);
    }

    public static function merge(array $ar1, array $ar2) {
        $pa1 = 0;
        $pa2 = 0;

        $c1 = count($ar1);
        $c2 = count($ar2);

        $result = [];

        while ($pa1 < $c1 && $pa2 < $c2) {
            if ($ar1[$pa1] <= $ar2[$pa2]) {
                $result[] = $ar1[$pa1++];
            } else {
                $result[] = $ar2[$pa2++];
            }
        }
        if ($pa1 >= $c1) {
            while ($pa2 < $c2) {
                $result[] = $ar2[$pa2++];
            }
        } else {
            while ($pa1 < $c1) {
                $result[] = $ar1[$pa1++];
            }
        }

        return $result;
    }
}