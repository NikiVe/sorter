<?php

namespace Sorter;

class ShellSort extends Sorter
{
    protected $func;

    public function __construct($func = null) {
        $this->func = $func ?: static function($size) { return self::getGapsShell($size);};
    }

    public function sort(int $size, array $array) {
        $gaps = call_user_func($this->func, $size);

        foreach ($gaps as $gap) {
            for ($i = 0; $i + $gap < $size; $i++) {
                $j = $i + $gap;

                $back = 0;
                $temp = $array[$j];
                $setPosition = $j;

                while ($i - $back >= 0 && $array[$i - $back] > $temp) {
                    $array[$j - $back] = $array[$i - $back];
                    $setPosition = $i - $back;
                    $back += $gap;
                }
                $array[$setPosition] = $temp;
            }
        }

        return $array;
    }

    public static function getGapsShell(int $size) {
        $result = [];

        $func = function ($n, $k) {
            return (int)($n / (2 ** $k));
        };

        for ($k = 1;; $k++) {
            $gap = $func($size, $k);
            if ($gap === 0) {
                break;
            }
            $result[] = $gap;
        }
        return $result;
    }

    public static function getGapsFrankAndLazarus(int $size) {
        $result = [];

        $func = function ($n, $k) {
            return (int)(2 * $n / (2 ** ($k + 1 )));
        };

        for ($k = 1;; $k++) {
            $gap = $func($size, $k);
            if ($gap === 0) {
                break;
            }
            $result[] = $gap;
        }
        return $result;
    }

    public static function getGapsKnuth(int $size) {
        $result = [];

        $func = function ($k) {
            return (int)(((3 ** $k) - 1) / 2);
        };

        for ($k = 1;; $k++) {
            $gap = $func($k);
            if ($gap > $size / 3) {
                break;
            }
            $result[] = $gap;
        }
        return array_reverse($result);
    }

    public static function getGapsSedgewick(int $size) {
        $result = [1];

        $func = function ($k) {
            return (int)(4 ** $k + 3 * 2 ** ($k - 1) + 1);
        };

        for ($k = 1;; $k++) {
            $gap = $func($k);
            if ($gap > $size) {
                break;
            }
            $result[] = $gap;
        }
        return array_reverse($result);
    }
}