<?php


namespace Sorter;


class HeapSort extends Sorter
{
    protected $array = [];

    public function sort(int $size, array $array) {
        $this->array = $array;
        for ($root = (int)($size / 2) - 1; $root >= 0; $root--) {
            $this->moveMaxToRoot($root, $size);
        }

        for ($i = $size - 1; $i >=0; $i--) {
            $this->array = self::swap($this->array, 0, $i);
            $this->moveMaxToRoot(0, $i);
        }
        return $this->array;
    }

    public function moveMaxToRoot(int $root, int $size) {
        $l = $root * 2 + 1;
        $r = $l + 1;

        $x = $root;

        if ($l < $size && $this->array[$x] < $this->array[$l]) {
            $x = $l;
        }
        if ($r < $size && $this->array[$x] < $this->array[$r]) {
            $x = $r;
        }

        if ($x === $root) {
            return;
        }

        $this->array = self::swap($this->array, $root, $x);
        $this->moveMaxToRoot($x, $size);
    }

}