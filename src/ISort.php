<?php


namespace Sorter;


interface ISort
{
    public function sort(int $size, array $array);
}