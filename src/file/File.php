<?php


namespace Sorter\file;


abstract class File
{
    public function __destruct() {
        fclose($this->handle);
    }

    public function remove() {
        unlink($this->path);
    }
}