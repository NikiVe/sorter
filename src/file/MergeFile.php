<?php


namespace Sorter\file;


class MergeFile
{
    protected $path;

    public function __construct($path) {
        $this->path = $path;
        if (file_exists($this->path)) {
            unlink($this->path);
        }
    }

    public function get() {
        if (file_exists($this->path)) {
            $reader = new Reader($this->path);
            $ar = $reader->getAll();
            $reader->remove();
        } else {
            $ar = [];
        }
        return $ar;
    }

    public function save(array $ar) {
        $writer = new Writer($this->path);
        $writer->saveArray($ar);;
    }
}