<?php


namespace Sorter\file;


class Writer extends File
{
    protected $name;
    protected $handle;

    public function __construct(string $name) {
        $this->name = $name;
        $this->handle = fopen($name, 'w+');
    }

    public function saveArray(array $array) {
        foreach ($array as $item) {
            $this->saveItem($item);
        }
    }

    public function saveItem(int $item) {
        fwrite($this->handle, pack('I', $item));
    }
}