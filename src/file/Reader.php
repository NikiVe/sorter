<?php


namespace Sorter\File;


class Reader extends File
{
    protected $path;
    protected $handle;
    protected $size;

    public function __construct($path) {
        $this->path = $path;
        $this->handle = fopen($this->path, 'r');
        $this->size = filesize ($this->path);
    }

    public function getAll() {
        $res = [];

        if ($this->handle) {
            while (ftell($this->handle) < $this->size) {
                $bimNum = fread($this->handle, 4);
                $num = unpack('I', $bimNum);
                if (isset($num[1])) {
                    $res[] = $num[1];
                }
            }
        }
        return $res;
    }

    public function getSize() {
        return filesize($this->path);
    }

    public function getElementsCount() {
        return filesize($this->path) / 4;
    }

    public function getNext(int $size) {
        $tSize = $size;
        $res = [];
        for ($i = 0; $i < $tSize; $i++) {
            if (ftell($this->handle) >= $this->size) {
                break;
            }
            $bimNum = fread($this->handle, 4);
            $num = unpack('I', $bimNum);
            if (isset($num[1])) {
                $res[] = $num[1];
            }
        }
        return $res;
    }
}