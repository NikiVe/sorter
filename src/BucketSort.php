<?php


namespace Sorter;


class BucketSort implements ISort
{
    protected array $buckets = [];

    public function sort(int $size, array $array) : array {
        $result = [];
        $count = count($array);
        $this->buckets = self::initBuckets($count);
        $max = max($array);

        foreach ($array as $item) {
            $n = self::getBucketNum($item, $count, $max);
            $this->addToBucket($n, $item);
        }

        foreach ($this->buckets as $bucket) {
            foreach ($bucket as $item) {
                $result[] = $item;
            }
        }

        return  $result;
    }

    private function addToBucket(int $n, int $num) {
        $stack = $this->buckets[$n];
        $position = 0;
        foreach ($stack as $i => $v) {
            if ($num < $v) {
                $position = $i;
                break;
            }
        }

        array_splice($stack, $position, 0, $num);
        $this->buckets[$n] = $stack;
    }

    public static function getBucketNum(int $x, int $m, int $max) : int {
        return (int)(($x * $m) / ($max + 1));
    }

    public static function initBuckets(int $count) : array {
        $array = [];
        for ($i = 0; $i < $count; $i++) {
            $array[$i] = [];
        }
        return $array;
    }

}