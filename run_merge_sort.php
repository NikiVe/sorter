<?php

use DataStructure\ArrayList;
use Sorter\NumberGenerator;
use Sorter\Runner;
use Tester\Tester;

include_once __DIR__ . '/vendor/autoload.php';

Tester::start(__DIR__ . '/sorting-tests/3.revers/', new Runner(new \Sorter\MergeSort()), 1);

