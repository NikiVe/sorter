<?php


namespace Test\unit;


use PHPUnit\Framework\TestCase;
use Sorter\MergeSort;

class MergeSortTest extends TestCase
{
    public function testMerget() {
        self::assertEqualsCanonicalizing([0, 1], MergeSort::merge(0, 1, 1, [1, 0]));
        self::assertEqualsCanonicalizing([0, 1], MergeSort::merge(0, 1, 1, [0, 1]));

        self::assertEqualsCanonicalizing(
            [0, 1, 2, 3, 5, 6, 8, 9],
            MergeSort::merge(0, 3, 8, [0, 1, 5, 9, 2, 3, 6, 8])
        );

        self::assertEqualsCanonicalizing(
            [0, 1, 2, 3, 5, 6, 8, 9],
            MergeSort::merge(0, 3, 8, [0, 1, 2, 3, 5, 6, 8, 9])
        );

        self::assertEqualsCanonicalizing(
            [0, 1, 2, 3, 5, 6, 8, 9],
            MergeSort::merge(9, 9, 9, [0, 1, 2, 3, 5, 6, 8, 9])
        );

        self::assertEqualsCanonicalizing(
            [0, 1, 2, 3, 5, 6, 8, 9],
            MergeSort::merge(0, 0, 0, [0, 1, 2, 3, 5, 6, 8, 9])
        );

        self::assertEqualsCanonicalizing(
            [0, 1, 2, 3, 5, 6, 8, 9],
            MergeSort::merge(0, 4, 7, [0, 1, 2, 3, 5, 6, 8, 9])
        );
    }

    public function testSort() {
        $sorter = new MergeSort();
        self::assertEqualsCanonicalizing([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], $sorter->sort(10, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]));
        self::assertEqualsCanonicalizing([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], $sorter->sort(10, [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]));
    }
}