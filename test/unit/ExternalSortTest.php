<?php


namespace Test\unit;


use PHPUnit\Framework\TestCase;
use Sorter\ExternalSort;
use Sorter\MergeSort;

class ExternalSortTest extends TestCase
{
    public function testMerget() {
        self::assertEqualsCanonicalizing([], ExternalSort::merge([], []));

        self::assertEqualsCanonicalizing([0, 1], ExternalSort::merge([0], [1]));
        self::assertEqualsCanonicalizing([0, 1], ExternalSort::merge([1], [0]));

        self::assertEqualsCanonicalizing([0, 1, 2, 3, 5, 6, 8, 9], ExternalSort::merge([0, 1, 2, 3], [5, 6, 8, 9]));

    }
}